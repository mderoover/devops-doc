# ELK - stack

## What is it

Elasticsearch is a search engine based on the Lucene library. It provides a
distributed, multitenant-capable full-text search engine with an HTTP web
interface and schema-free JSON documents. Elasticsearch is developed in Java.
Following an open-core business model, parts of the software are licensed under
various open-source licenses (mostly the Apache License), while other
parts fall under the proprietary (source-available) Elastic License. Official
clients are available in Java, .NET (C#), PHP, Python, Apache Groovy, Ruby and
many other languages. According to the DB-Engines ranking, Elasticsearch is
the most popular enterprise search engine followed by Apache Solr, also based on
Lucene.

logstash is a tool that gets the logs and parses the logs into a better format
to be saved and worked with.

Kibana is an open source data visualization dashboard for Elasticsearch. It
provides visualization capabilities on top of the content indexed on an
Elasticsearch cluster. Users can create bar, line and scatter plots, or pie
charts and maps on top of large volumes of data.

The combination of elasticsearch, logstash and kibana is referred as the ELK stack.

## Why should we use it

It makes logging of one or more applications easier and more visual. It makes
log analysis more enjoyable. The log data is stored centralized which makes
fetching the logs of a distributed system easier. It also extracts the most
interesting facts form the logs without any setup.

## How do you use it

### ElasticSearch

The most easy way to get up and running with elastic search is using docker if it is installed. First the image can be downloaded with:

``` bash
$ docker pull elasticsearch:7.6.2
<Pulling image output>
```

Running the image can with first creating a network and later run the image with
the following options:

``` bash
$ docker network create elastic-net
<CUSTOM_HASH>
$ docker run -d --name elasticsearch --net elastic-net \
    -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" \
    --name elasticsearch-instance elasticsearch:7.6.2
<CUSTOM_HASH>
```

This will run the container in the background and logs can be checked with:

``` bash
$ docker logs elasticsearch-instance
<LOGS OUTPUT>
```

### Kibana

For kibana the docker image is also used to run this example

``` bash
$ docker pull kibana:7.6.2
$ docker run -d --name kibana-instance --net elastic-net \
    -p 5601:5601 kibana:7.6.2
```

The kibana instance can be accessed by browser via http://localhost:5601


### combination

The data can be pushed to the ElasticSearch database by a REST api that is
located at the port 9200 by default. By fetching a GET operation on the URL
http://<ELASTICSEARCH_LOCATION>:9200

## References

* [Elasticsearch - wiki](https://www.wikiwand.com/en/Elasticsearch)
* [Kibana - wiki](https://www.wikiwand.com/en/Kibana)
* [What is ELK stack - youtube](https://www.youtube.com/watch?v=MRMgd6E9AXE)
