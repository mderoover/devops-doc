Based on Amazon's
[AWS Foundations: Getting Started with the AWS Cloud Essentials](https://www.aws.training/Details/Video?id=49639)
course and
[AWS Cloud Pratitioner Essentials (Second Edition)](https://www.aws.training/Details/Curriculum?transcriptid=LILuH-XqBkOLGgZIg7nBlQ2&id=27076#modules)


# AWS Management options
* AWS Management console
    * Graphical interface
    * Android and IOS application available
* CLI interface
    * Command line interface
* Software development kits (SDKs)
     * Access AWS from your code
* All options based on common API


# AWS Global Infrastructure
* High quality global network performance
* AWS Regions
    * Geographical area
    * 22 Regions
    * One or more availability zones
    * Resources are isolated to regions (No automatic data replication across
      regions)
    * Determine based on legality, client latency, available services, and
      costs
* AWS Availability zones
    * Contains one or more data center
    * Can still be chosen individually by the client
    * Are all interconnected with low latency and high reliability
    * Use multiple availability zones for improved reliability
* AWS Data center
    * Cannot be manually chosen by the client
    * The place where data resides and processing happens
* AWS Edge locations
    * Used for caching and improved performance world wide


# AWS Compute services
* Amazon Elastic Compute Cloud (EC2)
    * IaaS
    * Resizable compute capacity as VMs
    * Optimized instances available for: General purpose, Computing, Memory,
      Accelerated computing, and Storage
* Amazon EC2 Auto Scaling
    * Automatically add and remove EC2 instances
    * Helps you maintain application availability
    * Brings elasticity to EC2
* Amazon Elastic Container Service (ECS)
    * CaaS
    * Container orchestration service
    * Highly scalable, highly performant
    * Supports Docker containers
* Amazon EC2 Container Registry (ECR)
    * Docker container registry
    * Fully managed
* AWS Elastic Beanstalk
    * PaaS
    * Deploying and scaling web applications and services
    * Apache, Microsoft internet information services,...
* AWS Lambda
    * Serverless computing
    * Cost efficient
* Amazon Elastic Kubernetes Services (EKS)
    * CaaS
    * Kubernetes engine
    * Deploy, manage, and scale with Kubernetes
* AWS Fargate
    * Run containers without managing servers or clusters


# AWS Networking services
* Amazon Elastic load balancing
    * Distributes incoming network traffic to multiple targets
    * Works across a single or multiple availability zones
* Amazon Virtual Private Cloud (VPC)
    * Isolated logical portion of the AWS cloud
    * Comparable to virtual networks (support routing, subnets, security,...)
    * VPC limited to single region but can have multiple availability zones
    * Max 5 VPCs per region
    * Network address control list: firewall for subnets, controlling inbound
      and outbound traffic at subnet level
    * Security Groups: firewalls for EC2 instances
* AWS Direct Connect
    * Dedicated private network connection between AWS and own data center
* Amazon Route 53
    * Scalable DNS system
    * Route end users to internet applications in a reliable way
* AWS VPN
    * Secure private channel from your network to AWS global network


# AWS Storage services
* Amazon S3
    * Object storage
    * Store objects in Buckets
    * Designed for 99.999999999% durability
* Amazon Elastic Book Store (EBS)
    * Block storage
    * Offers persistent block storage for a single instance
    * Replication offers high availability and durability
    * Different drive types exists (SSD, HDD, cold HDD,...)
    * Can create volume snapshots
    * Can encrypt volumes
* Amazon Elastic File System (EFS)
    * File system storage
    * Can be used as a network file system
    * Meant to offer persistent storage to multiple instances
* Amazon S3 Glacier
    * Archiving storage
    * Durable and low cost
    * For long term storage and infrequently accessed data


# AWS Database services
* Amazon Relational Database Service (RDS)
    * Managed relational databases
    * Multiple engines (Amazon Aurora, PostgreSQL, MySQL, MariaDB,...)
* Amazon Aurora
    * Relational database engine for RDS
    * Up to 5x faster than standard MySQL Database
    * Up to 3x faster than standard PostgreSQL Database
    * Continues backup to S3
    * Reduce database cost up to 90%
* Amazon DynamoDB
    * Managed NoSQL Database service
    * Deliver low latency at any scale
* Amazon Redshift
    * Data warehouse
    * Fast and scalable
    * Analyze data (run queries)
* Amazon DocumentDB
    * MongoDB-compatible database
* Amazon Neptune
    * Graph database


# AWS Security services
* AWS Identity and Management Access (IAM)
    * Create and manage AWS Users and Groups
    * Manage authentication and authorization
    * Manage resource permissions
* AWS Organizations
    * Restrict actions allowed in your AWS accounts
* Amazon Cognito
    * User sign-up + sign-in
    * Access control to your web and mobile applications
* AWS Artifact
    * On demand access to security and compliance reports
* AWS Key Management Service (KMS)
    * Create and manage keys
* AWS Shield
    * Managed DDoS (Distributed Denial of Service) protection
* AWS shared responsibility model
    * AWS responsible for some parts security
    * Customer responsible for some parts of security
    * Client is responsible for encryption of data in rest and in transit
    * Client is responsible for configuring the network for security
    * Client is responsible for safety of registers and logins
    * Client is responsible for configuration of security groups


# AWS Pricing
* Principles
    * Pay as you go
    * Save when you reserve
    * Pay less by using more
* On-demand instances
    * Charged per hour
    * short term
* Reserved instances
    * Discount for 1-3 year commitments (up to 75%)
    * For applications with steady usage
* Dedicated hosts
    * For applications with specific compliance requirements
* Spot instances
    * Spare AWS capacity for up to 90% discount
    * Applications with flexible start and end times
* AWS Free tier
    * Available for 12 months after account creation
    * Also always free services available
