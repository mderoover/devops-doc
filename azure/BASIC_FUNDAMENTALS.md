# Cloud Computing Concepts

## Terminology
* High availability \
What percentage of the time does your system response to user requests properly.
Percentage of uptime.

* Scalability \
You websites ability to grow as the number of concurrent users grows.
If it takes long to grow your system, is is not scalable.

* Elasticity \
Ability to automatically grow and shrink based on demand. Closely linked to
scalability.

* Agility \
Ability to respond quickly to a rapidly changing market.

* Fault Tolerance \
The ability of a system to handle faults like power, networking, or hardware
failures. A high fault tolerance improves your availability.

* Disaster recovery \
The ability of a system to recover from failure (disaster, flood,...)  within a
period of time and what data is lost.

* Economies of scale \
Economy concept that says: the more you purchase, the better your buying power.
Essentially it means that Microsoft can always run a server cheaper than you
can yourself.

* Capital expenditure (CapEx) and Operational Expender (OpEx) \
These are financial terms.  CapEx is seen as an investment in assets (like
computers) that return investment over time. OpEx is the cost of your operating
expenses. You pay your expenses as they come in. OpEx is preferred compared to
CapEx.

* Consumption-Based model \
Cloud is mostly like this. You pay for what you use. Charged per
time/execution/storage/...

* Paradigms for hosting application \
You could simply buy a cloud server and run your application there, but there
are also alternatives.

* Infrastructure as a surface (IaaS) \
Virtual machines, networking, load balancers, firewalls,...

* Platform as a service (PaaS) \
Upload code packages and let the cloud run them without direct access to the
hardware. This is typically done for web-apps.

* Software as a service (SaaS) \
Software you'd typically install on your own server, but instead offered as a
cloud service. You only have access to the configuration.


## Public, Private, and Hybrid cloud
* Public cloud \
Computing services offered over the internet. Available to the public.

* Private cloud \
Computing offered only to select users; internal or corporate cloud.
You have to go through a sales process.

* Hybrid cloud \
Supplement your private cloud with public services. You use the public cloud for
scalability and elasticity.


# Azure Core Services
* Regions \
54 Different regions spread across the world where Azure computing services are
available. Some areas are under-served (Africa...). Microsoft has the best
region coverage in the world compared to AWS for example. Not all regions are
available for everyone. Some regions are linked to governments for example.

* Availability zones \
Existing out of multiple data centers. Only available in some regions. With
availability zones you can specify in which data center in the region you want
your server to run to maximize your availability. (Do not put all your eggs in
one basket)

* Resource groups \
Resource groups are literally a way to logically group resources. Also allows
you to give you policy enforcement on top of resources.

* Azure Resource Manager (ARM) \
A common API to interface with Azure. If you use the portal or command line or
whatever it all uses the same API so you have the same features everywhere.
Allows you to interface with Azure in a consistent way and allows you a bunch of
different benefits.


## Specific Core products offered by Azure
### Compute
* Virtual Machines \
Infrastructure as a service. Allows you to run a Linux of Windows machine in the
cloud on top of a Hyervisor.

* Virtual Machine Scale Sets \
Gives elasticity to a set of virtual machines. Offers load balancers for
example. Multiple virtual machines acting as if they are one.

* App Service \
Platform as a service. Create your application, zip it, upload it to Azure.
Azure just runs your app services as code. Things like auto healing and load
balancing are provided as well.

* Functions \
Serverless model. You have absolutely no control about on which server your
application is running. You can even code your application in the browser.
Functions are small, they are meant to be used to execute small tasks. They are
mostly cheaper than having a complete virtual machine but the development
process is completely different for serverless application.

* Azure Container Instances - ACI \
The quickest way to get a container created according to Microsoft. However they
do not have a lot of scalability capabilities. It is not designed to be elastic
or to scale, but it is quick.

* Azure Kubernetes Services - AKS \
Containerized applications but with scalability! Kubernetes has become an
industry standard.


### Networking
* Virtual Network \
Interconnecting the virtual machines to each other.

* Load Balancer \
Distributes traffic across virtual machines according to some algorithm.

* VPN Gateway \
If you want your virtual network to be accessible to your own company but not to
the public. You will need a private network to interconnect your own network
with the virtual network from Azure.

* Application Gateway \
Essentially a more sophisticated type of load balancer. A load balancer is
restricted to balancing traffic across servers. An application gateway can
understand incoming traffic and decide which server should handle it based on
the incoming request. (Distribute traffic based on URL's for example).

* Content Delivery Network (CDN) \
Servers that you do not control but that store static files that you want to
distribute to your users. This helps reduce the traffic to your servers and
respond to traffic much faster. (It can really speed up your application)


### Storage
* Azure Storage - Blob, File, Table, Queue \
Main storage method.

* Managed Disk \
Allow you to mount these disks to a virtual machine. (Virtual hard disks)

* Backup and Recovery Storage \
Archive storage.


### Databases
* Cosmos DB \
Globally available, highly responsive. A modern type of storage.

* Azure SQL Database \
You can still run your own SQL database on a virtual machine but using Azure SQL
Database is a better alternative for that. This is far more scalable. You don't
have to manage as much.

* Azure Database for MySQL \
Manages version for MySQL.

* Azure Database for PostgreSQL \
Managed version for PostgreSQL.

* Azure Database Migration Service \
Migrate between services. Migrate from AWS to Azure for example.

* Azure Synapse Analytics (Formerly SQL Data Warehouse) \
This is a reporting service.


## Core Azure Solutions
### Internet of Things (IoT)
* IoT Hub
* IoT Central


### Big Data and Analytics
* Azure Synapse Analytics (formerly SQL Data Warehouse)

* HDInsight \
A collection of Apache Hadoop services. (HD stands for Hadoop)

* Azure Databricks (brand new) \
A centralized service where you can pull in data from external sources,
manipulate that data, and create pretty reports with that data.


### Artificial Intelligence (AI)
* Azure Machine Learning Services
* Studio


### Serverless Model
This is essentially the highest level, you just upload code. You don't have to
worry about servers, Microsoft makes you a performance guarantee.
* Azure Functions
* Logic Apps
* Event grid


## Azure Management Tools
How to interact with Azure. Create, manage, and delete Azure resources.
* Azure CLI \
Based on the bash platform. You can make scripts to automatically setup
resources.

* PowerShell \
Connect to Microsoft Azure from PowerShell. Aimed at Windows of course.

* Azure Portal \
Point and click adventure.

* Azure Cloud Shell \
Command line tools build into the browser.

* Azure Advisor \
When you are in the portal it analyzes your use of resources. It makes you
recommendations based on your usage.


# Understanding security, privacy, compliance, and trust
## Network Security
* Azure Firewall \
Analyze traffic that is directed to it, and either reject traffic or allow
traffic through. Embedded in the application gateway as Web Application Firewall
WAF. Stops for example XSS attacks and SQL Injection.

* Azure Distributed Denial of Service (DDoS) Protection \
There is a basic option and a standard option. With the standard option you get
extra logging, alerting, telemetry, resource cost scale protection.

* Network security group (NSG) \
Can be applied to a virtual network. It's basically a set of rules that you can
apply. It can allow inbound traffic based on certain rules or prevent outbound
traffic based on certain rules. You can have multiple NSG's in a single virtual
network to for example allow traffic to your frontend and deny public traffic to
your backend. Inbound NSG rules protect a destination IP address and port. You
can also add VM's to a NSG by role instead of IP.

* User Defined Routes (UDR) \
This can be used to force traffic over a route. It can be used for example to
ensure all traffic goes through the firewall. You can also force traffic across
your corporate network if you have a VPN Gateway setup.

### Best Practices
* All virtual network subnets should use NSG (deny as much traffic as you can)
* DDoS - only if needed or after you have been attacked in the past
* Application Gateway with WAF - Enterprise grade applications should have
  firewalls.
* Security through layers - Make sure you have security in layers.


## Azure Identity Services
How does a user prove to the application that they are who they say they are?

* Authentication \
A user has proven who they are (user id + password)

* Authorization \
Is this user permitted to perform an action?

Not all authenticated users should be admin users!


### Azure Active Directory (AD)
Identity as a service (IDaaS). This is Microsoft's preferred solution  for
identity management. Complete solution for managing users, groups, roles,...
It also enables the concept of single-sing on. This means that the user can use
the same id and password in every application. A centralized identity! You can
synchronize Azure AD with a Corporate Active Directory.


### Azure multi-factory Authentication
This is a feature embedded in Azure AD that you can optionally enable.
First factor is your ID, second factor is you password, and the third factor is
typically your phone (SMS, authenticator app, phone call,...).

An order of magnitude more secure than simple a username and a password.


## Security Tools and Features
Azure encapsulates both physical and digital security under security. Physical
security

* Azure AD \
see earlier

* Multi-Factor Authentication (MFA) \
see earlier

* Role-Based Access Control (RBAC) \
This is the recommended authentication system.

* Layered security

    * Data \
        Virtual network endpoint system, restrict the SQL database to a specific
        network.
    * Application \
        API management system
    * Compute \
        Limit remote desktop access, Windows update,...
    * Network \
        NSG, use of subnets, deny by defaults.
    * Perimeter \
        DDoS, Firewalls
    * Identity and Access \
        Azure AD
    * Physical \
        Door locks and key cards.

* Azure Security Center \
A unified security management and advanced threat protection. There is a free
tier and a paid tier for this solution.

* Key Vault
A central repository for all of your secrets, certificates, and signing keys.

* Azure Information Protection (AIP) \
Protecting your documents. Apply labels to emails and documents with particular
permissions. Only people with the necessary permissions can see the documents.
Can protect documents from being viewed, shared, and even printed.

* Azure Advanced Threat Protection (ATP) \
Monitor and profile user behaviour and activities. Linked with Azure AD. It
notices when something unusual happens. Protects user identities and reduces
attack surfaces. For example protects against brute force.


## Azure Governance Methodologies
* Azure policy \
Implement standards for your application across Azure. Either make hard rules
are set a standard and evaluate compliance. You can choose for example which
regions your virtual machines have to run on. You can choose which virtual
machines your developers are allowed to use. There are many different rules you
can apply. You can also create custom policies.

* Policy initiative \
A set of grouped policies. This can be used to more easily apply a large set of
policies.

* Role-Based Access Control (RBAC) \
Microsoft's recommended solution for authorization. Give access to resources at
the role level. You can than assign employees to the roles. There are also
build-in roles, the most common of which are Reader, Contributor, Owner. Every
resource has its own set of roles as well.

* Azure Locks \
Tie down resources and flag that they shouldn't be changed. There are two major
kind of locks. A Read-Only lock and a Can-not-Delete lock. Can be used to ensure
you do not accidentally delete production servers for example. With RBAC you can
allow only specific roles to delete locks.

* Azure Advisory Security Assistance

* Azure Blueprints \
You can create a subscription template with predefined roles and permissions
which you than use to create other subscriptions.


## Monitoring and reporting options
* Azure Monitor \
This gathers events from all tools of Azure and adds them to log files or
measures. There are tons of ways to get your data back out of Azure Monitor.

* Azure Surface Health \
This tells what is going on in the world in Azure. Are there any service issues.
Is there any maintenance planned. Things like that. Azure surface health is not
specific to you but about Azure in general.


## Privacy Compliance and Data Protection standards
Think about terms like GDPR, ISO, and NIST. Azure is in compliance with a lot of
standards but not all of them. Check this if you need it! Azure also offers
tools to help you comply with the standards.
* GDPR - European citzen privacy protection standard
* ISO - Internationalized standards
* NIST - Cybersecurity framework

* Compliance manager \
You can see how well you are complying to standards. It is like a checklist you
can follow.


# Azure pricing and support
## Azure subscriptions
* Azure Subscriptions \
A subscription can be seen as a billing unit, bundling up the cost of all
resources under it. A subscription will be billed to the owner of that
subscription. A user can be part of multiple subscriptions. An example could be
to have a separate subscription for HR, Finance, and Marketing. Each
subscription can have a separate set of services linked to it.

* Azure management groups \
With this you can create a hierarchy of subscriptions. You can bundle
subscriptions together in groups and manage them on the group level.


## Planning and Management of costs
* Pay as you go \
This uses your credit card and by the end of the month your bill gets
subtracted from your credit card. You can put limits in it as well.

* Enterprise subscription \
This is an agreement you negotiate with Microsoft which you can do if you are a
big enough company. It is usually down to an annual bases.

* Microsoft Cloud Solution Provider (CSP) \

* Azure Free Account \
[link](https://azure.microsoft.com/en-ca/free/)

* Factors affecting costs \
Different services are billed based on different factors. There are services
billed pay per usage. For example Azure Functions, Logic Apps, Storage, outbound
bandwidth, Cognitive Services API. Other services are pay per time. For example
virtual machines. For virtual machines you can reserve it in advance for a year
for example which can result in discounts.

* Pay for bandwidth \
You only pay for outbound. 5 GB is free.
Transferring 1 peta byte of data costs $52000...

* Pricing calculator \
This can be used to estimate pricing.

* Total Cost of Ownership (TCO) \
The cost of a server is more than just the cost of the hardware.

* Azure cost management \
Analyze historical spending

### Best practices to minimize cost
* Azure advisor cost tab \
It tells you about resources you are paying for that are not being used.

* Auto-shutdown dev/qa resources \
Shutting down these resources out of office hours.

* Utilize cool/archive storage where possible \
90% storage costs.

* Reserved instances \
Can save up to 20 - 40% when you reserve for years.

* Configure alerts \
When billing exceeds a limit.

* Azure policies \
Restrict access to expensive services.

* Autoscaling resources \
Scale down your resources as required.

* Downsize resources when over-provisioned \
If you are running on a too powerful server, downsize it.

* Ensure every resource has an owner (tag)
Make sure you know who is responsible for resources.


## Support options available in Azure
* Basic support \
Self-help support, documentation, azure advisor. You do not have anyone you can
contact at Microsoft.

* Developer support - $29/month \
You get access to support during business hours. You can open up unlimited
tickets with 8 hours or less response time.

* Standard support - $100/month \
Multiple levels of severity tickets.

* Professional Direct - $1000/month \
Best practice guidance. You have a delivery manager which you can always
contact.

* Premier - Contact us (no pricing given) \
Across all Microsoft products. You will also have a technical account manager.


### Contacting support
You can open a support ticket using the online Portal. Depending on your plan
you can also email or phone them.


### Knowledge Center
There is a knowledge center with frequently asked questions.


## Azure Service Level Agreements (SLA)
It is financial guarantee that the service that they advertise will work.

* Composite SLA's \
When multiple SLA's cover a single solution (database, VM,...). You have to
multiply the reliability of the various SLA's to get your total reliability
(will be lower than the lowest reliability). Redundancy improves your
reliability.


## Service Lifecycle in Azure
* Preview features \
You shouldn't really use these. They can change significantly before they go
live.

* Public preview \
Available for everyone.

* Private preview \
You have to sign up for this.

* General availability \
From now on you can rely to it.



