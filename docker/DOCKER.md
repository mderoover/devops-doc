# What is it
Docker is the industry standard for creating and running containerized
applications. Docker aims to eliminate the "It works on my laptop" problem.


# Why should we use it
Docker is used to containerize applications. Containerized application are
lightweight, standalone, executable packages of software that include everything
needed to run an application: code, runtime, system tools, system libraries, and
settings.

Using containerized applications is safer and more reliable. They are isolated
from the host environment and can run on nearly any host machine. Containerized
applications also accelerate your workflow since you no longer need to spend
time reproducing an environment. The preconfigured environment is included in
your application.

Compared to a full blown virtual machine, docker is much more lightweight. This
is because a virtual machine used a guest OS which needs to virtualise resources
using a Hyervisor. Docker on the other hand runs natively on your host machine
without a guest OS.


# How do you use it


## Defining a Docker image
Docker images are defined using **Dockerfiles**.
``` Dockerfile
# Use the official image as a parent image.
FROM node:current-slim

# Set the working directory.
WORKDIR /usr/src/app

# Copy the file from your host to your current location.
COPY package.json .

# Run the command inside your image filesystem.
RUN npm install

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 8080

# Run the specified command within the container.
CMD [ "npm", "start" ]

# Copy the rest of your app's source code from your host to your image filesystem.
COPY . .
```
The complete syntax for Dockerfiles can be found [here](https://docs.docker.com/engine/reference/builder/).

There are also some [best
practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
when creating Dockerfiles. Some of the most important best practices are
summarized here:
* Create **ephemeral containers**
* Minimize your **build-context** (if not possible, use dockerignore files)
* Use **multi-stage builds**
* One container - One goal - One application
* Sort multi-line arguments alphabetically
* Use official starting image - preferably Alpine
* Use labels in Dockerfiles
* COPY is preferred over ADD
* If an application does not need privileges, use USER
* Make sure you know how **docker caching** works (it will save you time)

To build an image based on a Dockerfile use:
```
docker build --tag <imagename>:<tag/version> <directory>
docker build --tag example:1.0.1 .
```
The Dockerfile needs to be at the root of the directory and everything inside
the directory is passed as build context.


## Running a Docker image
When running a Docker container there are many things you can account for:
mounting volumes on the container, publishing ports, passing environment
variables, executing commands,... To get a full rundown of how to correctly run
a container in your situation I suggest you checkout the official docker
[documentation](https://docs.docker.com/engine/reference/commandline/run/)
```
docker run --name hello-container example:1.0.1
```


## Sharing a Docker image
When working in a team, you will want to share your Docker images. You also need
to host your images somewhere if you want cloud applications to access them.
Docker images are hosted on registries. The most common registry is [Docker
Hub](https://hub.docker.com/) but it is even possible to have a registry on
premise with a [local registry](https://docs.docker.com/registry/deploying/).

To host your image on a registry you need to create a repository on the registry
for your image. This is dependent on the registry but rather straight forward to
do. A repository will have a tag linked to it which you can use to pull and push
to the repository. To push your image to the registry use:
```
docker tag example:1.0.1 registry/repository-tag
docker push registry/repository-tag
```

When you try to run something on a machine that does not have the image yet,
docker will automatically try to download the image from the registry:
```
docker run registry/repository-tag
```


## Tips and tricks


# References
* [Docker Hub](https://www.docker.com/products/docker-hub) - Docker container
  registry

