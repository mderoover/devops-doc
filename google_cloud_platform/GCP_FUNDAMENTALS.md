
# Big Picture
```
-------------------------------------------------------------------------------
|                               Management tools                              |
|-----------------------------------------------------------------------------|
|          |  Hybrid&multi-cloud  |  Api Management  |   Migration   |        |
|          |---------------------------------------------------------|        |
|          |                AI and Machine learning                  |        |
| security |---------------------------------------------------------| Devops |
|          |         Database          |       Data Analystics       | tools  |
|          |---------------------------------------------------------|        |
|          |      Compute      |     Storage      |      Network     |        |
-------------------------------------------------------------------------------
```

A project in GCP is a billable unit. It has a credit card linked to it. All
resources in GCP belong to a project. Projects can be organized in folders. Each
folder can belong to one and only one organization as well. An organization is
the highest hierarchy in GCP.

GCP resources can be interacted with using 4 distinct methods. A web console, a
cloud shell/SDK (command line interface), the mobile app, and a REST API.


# GCP Compute Services

## Overview
* Where code is deployed and executed
* PaaS: App engine
* IaaS: Compute Engine
* Orchestration platform (container based): Kubernetes Engine
* Serverless computing: Cloud Functions

## App Engine
* Platform as a Service: PaaS
* Managed by Google (Scaling, Load balancing, ....)
* You only supply your code
* 2 environments: standard and flexible
* standard environment: sandbox, less control
* flexible environment: using docker containers, more control.

## Compute Engine
* Infrastructure as a Service: IaaS
* Launch Linux and Windows Virtual Machines
* Machine types with variable CPU and RAM configuration
* Usually not persistent, can attach persistent storage (SSD)
* VMs are charged per second with a minimum charge of 1 minute
* Sustained use discounts can cut costs (If you are running your VM for a while)
* Committed use discounts can cut costs (Sign up for long term use, +1year)

## Kubernetes Engine
* Containers as a Service: CaaS
* Managed environment for deploying container applications
* Kubernetes is most used orchestration platform
* Node pool: mixing and matching different VM configurations
* Google Compute Engine used to deliver nodes to GKE
* The service is tightly integrated with other Google Cloud Platform Services:
  Load balancing, Stack Driver, Monitoring,...)

## Cloud Functions
* Functions as a Service: FaaS
* Serverless environment
* You don't provision and configure resources in advance
* Executes code in response to an event
* JavaScript, Python3 and Go
* One of the most efficient and economical way to run code in the cloud


# GCP Storage Services
* Three major kinds of storage
    * Object Storage
    * Block Storage
    * File Storage
* GCP can store:
    * Unstructured data
    * Folders and Files

## Cloud Storage
* Unified object storage service
* Store and receive objects through API
* High frequency and low frequency data access possible
* Can be stored in single region or multiple regions
* Storage classes
    * Standard: High frequency access
    * Nearline: Low frequency access (once a month)
    * Coldline: Accessed less than once a year (archiving)

## Persistent Disks
* Reliable block storage
* Meant to give GCE VMs persistent storage
* Each disk can go up to 64TB in size
* PDs can have one writer and multiple readers
* Both SSD and HDD supported
* Three storage types:
    * Zonal
    * Regional
    * Local

## Cloud Filestore
* Mimics your local file system
* Managed file storage service for applications
* Deliver a NAS-like file system interface and a shared file system
* Centralized, highly available file system for GCE and GKE
* Filestore file shares are available as mount points on the VMs
* Filestore has build-in zonal storage redundancy for data availability
* Data is always encrypted while in transit


# GCP Network Services

## Network Tiers
* Premium tier: Using Google's own backbone
* Standard tier: ISP Network based backbone
* Default and recommended is by GCP is premium tier option

## Cloud Load Balancing
* Distributed traffic across multiple GCE VMs in a single or multiple regions
* HTTPS load balancer: Global load balancer
* Network load balancer: Routing across multiple TCP and UDP endpoints in the
  same region
* Internal load balancer: Route to multiple app servers
* External load balancer: Route to appropriate regions (or locations)

## Virtual Private Cloud (VPC)
* Provides private networking for VMs
* This is a global resource (regions spanning) with regional subnets
* Each VPC is logically isolated from each other
* To intercommunicate between subnets you use Firewall rules (selectively route
  traffic)
* VPC can be interconnected using VPC peering (Share resources among VPCs)

## Hybrid Connectivity
* Seamlessly and securely interconnect a local data center to GCP
* Cloud Interconnect: Extend on-premise network with GCP (run modern cloud
  applications on local data centre) Use dedicated network to connect
* Cloud VPN: Connect on premise environment to GCP through public internet
* Peering: Enable direct access to Google cloud resources with reduced internet
  egress fee (egress fee = bandwidth charged for outbound connectivity)


# Identity and Access management
* Who has What Access to Which resource

## Cloud IAM Identity
* You can apply roles to:
    * Google accounts
    * Service accounts
    * Google groups
    * G Suite Domains
    * Cloud Identity Domains
    * AllAuthenticatedUsers
    * AllUsers (also includes anonymous users)

## Cloud IAM Permissions
* Permission determine the allowed operations on resources
* Correspond 1:1 with REST methods for GCP Resources
* Permission are assigned to roles (not identities)

## Cloud IAM Roles
* Primitive roles
    * Owner: 100% access to resource
    * Editor: can modify and add additional permission
    * Viewer: read only access
* Predefined roles (specific to the resource):
    * roles/pubsub.publisher
    * roles/compute.admin
    * roles/storage.objectAdmin
* Custom roles:
    * Collection of assorted set of permissions
    * Fine-grained access to resources

## Service Accounts
* A special Google account that belongs to an application or VM
* Give an application or VM access to Google cloud resources
* Associated with key-pairs used for authentication
* Two types:
    * Google managed: Predefined by Google
    * User managed: Custom
* Each service account is associated with one or more roles


# GCP Database Services

## Cloud SQL
* Fully managed RDBMS Service
* Simplifies setup, maintenance, management, and administration of database
  instances
* Support for three types of RDBMS:
    * MySQL
    * PostgreSQL
    * Microsoft SQL Server
* Always preferred over manual database instances

## Cloud Bigtable
* Managed NoSQL database service
* Sparsely populated table that can scale to billions of rows and thousands of
  columns
* Ideal for throughput intensive data-processing and analytics
* Alternative to manually running Apache HBase instance
* Used for MapReduce, stream processing, and machine learning operations
* Preferred database for big data

## Cloud Spanner
* Managed, scalable, relational database service
* Can horizontally scale across multiple regions (continents)
* Brings best of relational and NoSQL databases
* Data is replicated synchronously with globally strong consistency
* Three types:
    * Read-write
    * Read-only
    * Witness: Make sure the data is synchronized and replicated in most
      consistent form

## Cloud Memorystore
* Fully managed in memory database cache based on Redis
* Used for caching and acceleration performance
* Store up to 300GB and network throughput up to 12Gbps


# GCP Data and Analytic Services
* Ingesting, collecting, processing, and visualizing data

## Cloud Pub/Sub
* Used for ingesting data at scale
* Based on publication/subscription pattern
* Used for data staging to other services like Dataflow and Data storage

## Cloud Dataflow
* Used for data processing in stream and batch modes
* Based on Apache Beam
* Inbound data can be queried, processed, and extracted for target environment
* Can be integrated with Apache Kafka

## Cloud Dataproc
* Managed Apache Hadoop and Apache Spark cluster environment
* Automated cluster management

## Cloud Datalab
* An interactive tool for data exploration, analysis, visualization, and machine
  learning
* Build on-top of the open source Jupyter Notebooks platform (the industry
  standard for data visualization)

## BigQuery
* Serverless, scalable cloud data warehouse (can run data analysis and queries
  in the cloud)
* Has an in memory BI engine (data analysis) and machine learning build in
* Supports standard SQL dialect for querying
* Queries can process external data storages
* Automatically replicate changes to keep a seven-day history of changes
* Supports data integration tools like Informatica and Talend


# GCP AI and Machine learning services

## Cloud AI Building blocks
* Based on API (no difficult programming needed)
* Sight: Vision, Video
* Conversation: Dialog flow, Text to Speech, Speech to Text
* Language: Translation, Natural language
* Structured data: regression, classification
* Pre-trained, cannot be trained by yourself

## Cloud AutoML
* Enables training high-quality models without writing code
* Specific to business problems
* Can be trained by yourself

## Cloud AI Platform
* Meant for advanced use cases
* Custom machine learning models
* Build on top of Kubeflow (which runs on Kubernetes)
* Covers the entire spectrum of machine learning pipelines

## AiI Hub
* Hosted repository of plug-and-play AI components


# GCP DevOps Services

## Cloud source repositories
* Repository comparably to git and GitHub
* Scalable git repository
* Advantage: source code maintained very close to your deployment environment
* Can trigger automated build, tests, deployment, ...

## Cloud build
* Managed environment for build management
* CI/CD tool used for Google cloud platform
* Supports building software written in any language
* Tight integration with Cloud Source Repo, GitHub, and BitBucket
* Native docker integration, automatic deployment to Kubernetes and GKE
* Identify vulnerabilities through efficient OS package scanning

## Google container registry
* Single location to manage container images and repositories
* Store images close to GCE, GKE, and Kubernetes Clusters (faster)
* Secure private scalable registry in GCP
* Detects vulnerabilities in the early stages of the software deployment
* Automatic lock-down of vulnerable container images
* Automatic container build process based on code or tag changes

## Cloud Developer tools
* IDE plugins for mainsteam developer tools
    * IntelliJ
    * Visual Studio
    * Eclipse
* Enhanced developer productivity


# Additional GCP Services

## Cloud IoT Services
* Cloud IoT Core: Used if you have multiple sensors, actuators,.. that need to
  be integrated with the cloud
* Edge TPU: Hardware available to run AI in offline mode

## Cloud API Management
* Apigee API platform
* API analytics: helps you make informed business decisions
* Cloud endpoints: tools to manage API development

## Cloud Hybrid and Multi-Cloud Services
* Traffic director
* Stack-driver
* GKE on-prem

## Google Cloud migration tools
* Transfer appliance: bulk data transfer using physical appliance
* Migrate for compute engine: migrate existing VMs or physical machines to GCE
* BigQuery Data Transfer Service: Migrate third-party SaaS data to BigQuery data
  platform




