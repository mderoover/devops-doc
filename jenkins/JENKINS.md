# What is Jenkins
Jenkins is a self-contained, open source automation server which can be used to
automate all sorts of tasks related to building, testing, and delivering or
deploying software.
Jenkins can be installed through native system packages, docker or even run
standalone by any machine with a Java Runtime Environment (JRE) installed.

The main feature from Jenkins are **pipelines** which are used to automate
processes. A pipeline is composed out of multiple **steps**. Each step can be seen
as a single command which performs a single action. When all steps in the
pipeline are successfully completed, the pipeline is considered to be successfully
completed. Steps are generally bundles into **stages**. At the bare minimum, an
ordinary pipeline had three stages *build*, *test*, and *deploy*.

Note: Jenkins also has a modernized UI and user experience called **Blue Ocean**
Functionality wise this is similar to traditional Jenkins but it does look
cooler. There is generally no reason not to use the modernized UI.


# Why do we need Jenkins
As mentioned, Jenkins is an automation server which can handle tasks like CI/CD.
Asking why Jenkins is needed is equivalent to asking why automation and by
extension CI/CD is needed in a project.

It takes some time to setup but should increase the speed by which a team can
iterate over a product.


# Jenkins VS Gitlab CI
For **small projects** Gitlab CI is superior to Jenkins since Jenkins comes with
the overhead of having to setup and host a Jenkins server. Gitlab CI also
integrates very nicely with your Gitlab repository and docker images.
For **medium projects** Gitlab CI and Jenkins are very comparable. Even so my
preference is still on Gitlab CI since the support for job parallelization is
better and acyclic pipelines are possible while such structures are more
difficult if not impossible to realize in Jenkins.
For **larger projects** however Jenkins might be more interesting because of the
control it offers since it is self-hosted. Jenkins also offers an excellent
plugin library and extensibility. You can even write your own custom plugins.
**Performance** wise Jenkins and Gitlab CI are equivalent.

**Overall** if you do not need the many plugins of Jenkins, and have a Gitlab
instance available on which you can host your code, Gitlab CI is preferable over
Jenkins.

Note: Because of the extensibility of Jenkins there is a very realistic chance
of it becoming a *plugin hell*. It might be interesting to minimize your use of
plugins while using Jenkins.


# How do we use it
The main feature from Jenkins are **pipelines**. These pipelines are defined
using a `Jenkinsfile` at the root of your repository. This file is
comparable to the `.gitlab-ci.yaml` file from Gitlab.


## Steps
The base building block of a step in Unix-like systems is the `sh` command. This
command is used to execute a shell command in the pipeline. The windows
equivalent is the `bat` command, which is used to execute a batch command in the
Jenkins pipeline.
```
pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                sh 'echo "Hello World"'
                sh '''
                    echo "Multiline shell steps works too"
                    ls -lah
                '''
            }
        }
    }
}
```
Steps can also be wrapped to enable **retry** or **timeout** behaviour.
```
...
steps {
    retry(3) {
        sh './flakey-deploy.sh'
    }

    timeout(time: 3, unit: 'MINUTES') {
        sh './health-check.sh'
    }
}
...
```
Wrapper steps like timeout and retry can also be daisy chained together.

It is also possible to do **finishing actions** like **cleanup**. These can be
defined in the `post` section of the Jenkinsfile.
```
pipeline {
    agent any
    stages {
        stage('Test') {
            steps {
                sh 'echo "Fail!"; exit 1'
            }
        }
    }
    post {
        always {
            echo 'This will always run'
        }
        success {
            echo 'This will run only if successful'
        }
        failure {
            echo 'This will run only if failed'
        }
        unstable {
            echo 'This will run only if the run was marked as unstable'
        }
        changed {
            echo 'This will run only if the state of the Pipeline has changed'
            echo 'For example, if the Pipeline was previously failing but is now successful'
        }
    }
}
```


## The execution environment
The **agent** section is used to define where and how Jenkins should execute
the pipeline. This directive is **required** for all pipelines.

There are several types of execution environments you can define but the most
common one is using a docker container.
```
pipeline {
    agent {
        docker { image 'node:7-alpine' }
    }
    stages {
    ...
    }
}
```


## Environment variables
**Environment variables** can be set either globally or per stage.
```
pipeline {
    agent {
        label '!windows'
    }

    environment {
        DISABLE_AUTH = 'true'
        DB_ENGINE    = 'sqlite'
    }

    stages {
        stage('Build') {
            environment {
                CURRENTSTAGE = 'build'
            }
            steps {
                echo "Database engine is ${DB_ENGINE}"
                echo "DISABLE_AUTH is ${DISABLE_AUTH}"
                echo "CURRENTSTAGE is ${CURRENTSTAGE}"
                sh 'printenv'
            }
        }
    }
}
```
It is a bad idea to directly put **credentials** in the Jenkinsfile. How to
securely handle credentials can be found in the
[handbook](https://jenkins.io/doc/book/pipeline/jenkinsfile/#handling-credentials)

Jenkins also has some build in environment variables which you can use. More
about this can be found in the Jenkins
[documentation](https://jenkins.io/doc/book/pipeline/jenkinsfile/#using-environment-variables)


## Testing
Since most people dislike having to scan through entire log-files, Jenkins
offers a way to generate concise **test-reports** as long as your tests output
standardized test-files. **JUnit-style XML reports** is one of the supported
formats.
```
pipeline {
    agent any
    stages {
        stage('Test') {
            steps {
                sh './gradlew check'
            }
        }
    }
    post {
        always {
            junit 'build/reports/**/*.xml'
        }
    }
}
```
NOTE: A pipeline that has failed tests will not be reported as *failed* in the UI
but as *unstable*


## Artifacts
Files generated during the execution of a pipeline can be stored as
**artifacts**.
```
pipeline {
    agent any
    stages {
    ...
    }
    post {
        always {
            archiveArtifacts artifacts: 'build/libs/**/*.jar', fingerprint: true
        }
    }
}
```


## Notifications
The post section can be used to send various **notifications** to members of the
team to let them know what is going on with the pipeline.
```
post {
    failure {

        mail to: 'team@example.com',
             subject: "Failed Pipeline: ${currentBuild.fullDisplayName}",
             body: "Something is wrong with ${env.BUILD_URL}"

        hipchatSend message: "Attention @here ${env.JOB_NAME} #${env.BUILD_NUMBER} has failed.",
                    color: 'RED'

    }
    success {

        slackSend channel: '#ops-room',
                  color: 'good',
                  message: "The pipeline ${currentBuild.fullDisplayName} completed successfully."

    }
}
```


## Deployment
Jenkins also offers continues delivery/deployment. Note that a successful build and test
stage are important for a successful **deploy stage**. The
`skipStagesAfterUnstable` option can be useful in this regard.
```
pipeline {
    agent any
    options {
        skipStagesAfterUnstable()
    }
    stages {
        stage('Build') {
            steps {
                echo 'Building'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying'
            }
        }

        stage('Deploy - Staging') {
            steps {
                sh './deploy staging'
                sh './run-smoke-tests'
            }
        }
        stage('Deploy - Production') {
            steps {
                sh './deploy production'
            }
        }
    }
}
```


## User input
Sometimes **user input** is required to decide whether a pipeline may continue to
the next stage. This can be obtained with the `input` step.
```
pipeline {
    agent any
    stages {
        ...
        stage('Deploy - Staging') {
            steps {
                sh './deploy staging'
                sh './run-smoke-tests'
            }
        }
        stage('Sanity check') {
            steps {
                input "Does the staging environment look ok?"
            }
        }
        stage('Deploy - Production') {
            steps {
                sh './deploy production'
            }
        }
    }
}
```


# References
* [Basic Steps](https://jenkins.io/doc/pipeline/steps/workflow-basic-steps/)
* [Standard Environment Variables](https://jenkins.io/doc/book/pipeline/jenkinsfile/#using-environment-variables)
* [Handbook](https://jenkins.io/doc/book/getting-started/)
* [Getting Started](https://jenkins.io/doc/pipeline/tour/getting-started/)
* [CI/CD Comparison](https://www.digitalocean.com/community/tutorials/ci-cd-tools-comparison-jenkins-gitlab-ci-buildbot-drone-and-concourse)
* [CI/CD Comparison 2](https://www.katalon.com/resources-center/blog/ci-cd-tools/)

