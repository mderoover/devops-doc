# Jenkins - Hello World
The goal is to setup a Jenkins server with a linked agent and make the agent
print "Hello world" in an alpine docker image whenever a commit is made
to its repository.


## Setting up the server
First ensure that your firewall allows access to port `80`. After this you can
startup the server using the following command in the `jenkins` directory:
```
docker-compose up -d jenkins
```
You can get logs from the Jenkins server with:
```
docker logs jenkins
```
Now you can access Jenkins by surfing to your own public IP address.
* The first time you access this it asks after your administrator password. This
  can be found in the Jenkins logs.
* After giving your password it will ask you to install plugins. It's
  recommended to install the suggested plugins.
* The next step is to create you first admin user.
* When it asks you for instance configuration, simply accept the default.
* It is possible that at this point Jenkins asks you to restart. When restarting
  you might have to refresh your webpage.


## Setting up the agent (and linking to the server)
To startup the agent node you need to first identify the id the docker group on
your host. This can be found in `/etc/group`. Substitute the value of
`DOCKER_HOST_GROUP_ID` in the docker-compose file with your own value.
After this you can run the following command to launch the slave:
```
docker-compose up -d --build jenkins-slave
```
Now you can either use the ssh-keys generated by the image or supply your own
ssh keys. The keys can be found under `jenkins-slave/ssh`. Make sure the
`jenkins-slave/ssh/authorized-keys` file contains the public key you chose to
use. By default it contains the public key generated by the image.

To link your Jenkins-slave to the master you need to login into the Jenkins
interface using the administrator account. Node management is done under
`Manage Jenkins > Manage nodes and clouds`. To add the local agent follow the
next steps:
1. Add a `New Node`.
2. Give you node a name like `localagent`.
3. Choose how many jobs your node can de in parallel by changing the `#
   executors` (be sane and base yourself on the number of CPU's your node has).
4. Choose a remote root directory, for example `/home/jenkins/jenkins`0
5. As launch method, choose launch agents via ssh.
6. The hostname of your agent will be `jenkins-slave`. See docker-compose file.
7. Add a Jenkins credentials. Choose `ssh username with private key`. The
   username will be `jenkins` and you will enter the private key directly. For
   the private key you copy the content of the file corresponding to the public
   key you added to the slave. Leave the passphrase empty (unless you gave your
   ssh key a passphrase).
8. As `Host Key Verification Strategy` choose `Non Verifying Verification
   Strategy` to keep it simple.
9. Save and you should see your local agent coming online.

In case anything fails, the startup logs of the node can be used for debugging.

Once you have your local agent up and running, you can decrease the number of
executors on the master to 0 to ensure everything is executed on the local agent
instead.


## Create a pipeline
From the Jenkins interface, create a new item:
1. Choose the *Pipeline* item
2. Under Pipeline option, select Pipeline script from SCM.
3. Select *git* as SCM
4. `https://github.com/mderoover-old/jenkins-sample.git` has been prepared for
   this purpose, but you can use a different repository as well.
5. Make sure scriptpath is set to `Jenkinsfile`
6. You can manually run your pipeline by pressing `Build Now` from the menu on
   the left.
