# Jenkins
Jenkins is very plugin heavy. This is good since there will most likely already
be a plugin for anything you wish to do, and if there isn't you can make you own
plugin. On the other hand there is a very realistic risk to be buried under all
the plugins and just end up with a spaghetti that nobody understands.


# Gitlab
Compared to Jenkins, Gitlab uses a batteries included mentality. This prevents
the plugin spaghetti you can have with Jenkins. Gitlab also offers more than
only CI with your entire DevOps setup nicely integrated into a single tool.


# Hello world example
For both Gitlab and Jenkins a *hello-world* example has been created. These
examples detail the complete setup to run a master and slave in docker
containers, link this slave to the master, and have a trivial pipeline running
in an alpine docker container on the slave nodes.

For both of these this very doable and simple docker-compose files can be used
to have this up and running in no time. The entire process however is more
stream-lined in the case of Gitlab, which uses the *gitlab-runner* client
program on its slave node.


# Performance
The basic Jenkins server consumes less and starts up faster than the equivalent
Gitlab server. This however is understandable since the basic Gitlab server is
far more feature rich than a basic Jenkins server.


# Creating pipelines
Creating pipelines in both cases is very similar. For Gitlab you use the
`.gitlab-ci.yml` file and for Jenkins you have the `Jenkinsfile`. Both of these
are very similar and if you can use one you won't have any problems with the
other.

Jenkins also has an alternative way to setup pipelines using the scripting
language *Groovy*. This is a more advanced way to control your pipelines.
Gitlab does not support this by design since they want to have a simple learning
curve for mastering their CI.


# References
* [Install Gitlab using Docker Compose](https://docs.gitlab.com/omnibus/docker/#install-gitlab-using-docker-compose)
* [Install Gitlab Runner in Docker](https://docs.gitlab.com/runner/install/docker.html)
* [Install Jenkins in Docker](https://jenkins.io/doc/book/installing/#downloading-and-running-jenkins-in-docker)
* [Jenkins distributed build via ssh](https://wiki.jenkins.io/display/JENKINS/Distributed+builds#Distributedbuilds-Havemasterlaunchagentviassh)
* [Gitlab CI Syntax](https://docs.gitlab.com/ee/ci/yaml/)
* [Jenkins Declarative Pipeline Syntax](https://jenkins.io/doc/book/pipeline/syntax/#declarative-pipeline)
* [Jenkins Scripted Pipeline Syntax](https://jenkins.io/doc/book/pipeline/syntax/#scripted-pipeline)
