# Python + pyinstaller using Jenkins
This tutorial uses Jenkins (Blue Ocean flavor) in a docker image to build, test,
and deliver a python application. I assume you are using a Linux system. If you
are using anything else the official tutorial can be found
[here](https://jenkins.io/doc/tutorials/build-a-python-app-with-pyinstaller)


## Requirements
* At least a 1GB RAM
* 10 GB Drive space
* docker
* git


## Jenkins in Docker
Create a bridge adapter in docker specifically for Jenkins
```
docker network create jenkins
```
Create docker volumes for the docker client TLS certificates and to have
persistent data storage for Jenkins
```
docker volume create jenkins-docker-certs
docker volume create jenkins-data
```
In order to execute Docker commands inside Jenkins nodes, execute the following
docker container run command:
```
docker container run --name jenkins-docker --rm --detach \
  --privileged --network jenkins --network-alias docker \
  --env DOCKER_TLS_CERTDIR=/certs \
  --volume jenkins-docker-certs:/certs/client \
  --volume jenkins-data:/var/jenkins_home \
  --volume "$HOME":/home docker:dind
```
Run the **jenkinsci/blueocean** Docker image as a container using the following
docker run command
```
docker container run --name jenkins-tutorial --rm --detach \
  --network jenkins --env DOCKER_HOST=tcp://docker:2376 \
  --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 \
  --volume jenkins-data:/var/jenkins_home \
  --volume jenkins-docker-certs:/certs/client:ro \
  --volume "$HOME":/home \
  --publish 8080:8080 jenkinsci/blueocean
```
You can check whether the containers are up using:
```
docker container ls
```
Browse to `http://localhost:8080`. You will be asked for the administrator
password. This can be obtained from the Jenkins logs.
```
docker logs jenkins-tutorials
```
On the next screen is is possible to customize Jenkins with plugins.
We will select `install suggested plugins`.

Finally, Jenkins will ask you to create your first administrator user. Make a
new user. Save and continue. If you are asked to create a new instance, just
save and continue. And finally start using or restart Jenkins.


### Stopping and restarting
stopping:
```
docker container stop jenkins jenkins-docker.
```
restarting (same command as previously):
```
docker container run --name jenkins-tutorial --rm --detach \
  --network jenkins --env DOCKER_HOST=tcp://docker:2376 \
  --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 \
  --volume jenkins-data:/var/jenkins_home \
  --volume jenkins-docker-certs:/certs/client:ro \
  --volume "$HOME":/home \
  --publish 8080:8080 jenkinsci/blueocean
```


## Preparing the sample repository
We are going to use GitHub to host the sample repository.
1. Create a **fork** of the
   [simple-python-pyinstaller-app](https://github.com/jenkins-docs/simple-python-pyinstaller-app)
   repository under your own GitHub account.
2. Clone the forked repository to
   `/home/<your-username>/GitHub`


## Creating your pipeline project in Jenkins
1. Under welcome to Jenkins click **create new job**.
2. Specify a name, and use **pipeline** as type.
3. On the next page, select the pipeline tab.
4. From the definition field, choose **pipeline script from SCM**.
5. From the SCM field, choose git.
6. In the Repository URL, fill in the local path to the repo:
   `/home/GitHub/simple-python-pyinstaller-app`
7. Save your configurations.


## Creating your pipeline as a Jenkinsfile
Commit a Jenkinsfile at the root of your local repository containing the
following:
```
pipeline {
    agent none
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'python:2-alpine'
                }
            }
            steps {
                sh 'python -m py_compile sources/add2vals.py sources/calc.py'
                stash(name: 'compiled-results', includes: 'sources/*.py*')
            }
        }
    }
}
```
Most of this is familiar except the stash command. **Stash** saves the specified
files and makes them available for use in later stages.

Make sure you commit this file in your local git repo.


## Running your first pipeline
Once your Jenkinsfile has been committed to the local repo, you can go back to
Jenkins.
1. From Jenkins, click `open Blue Ocean`
2. You should see a `this job has not been run` message box. Click `Run` and
   immediately click `Open` in the lower-right.
3. From this screen you can see the log and execution of your pipeline.
4. Take some time to explore this screen and Blue Ocean.


## Adding a test stage to the pipeline
We are going to adapt the Jenkinsfile. Copy the following code snippet into your
local Jenkinsfile:
```
stage('Test') {
    agent {
        docker {
            image 'qnib/pytest'
        }
    }
    steps {
        sh 'py.test --junit-xml test-reports/results.xml sources/test_calc.py'
    }
    post {
        always {
            junit 'test-reports/results.xml'
        }
    }
}
```
Go back to Jenkins Blue Ocean and click the `Run` button to start your pipeline.
You can quickly click the `Open` button to go to the log or click on the correct
line in the list. You will notice that the additional test stage in present.

You can see the results from the tests under the `test` tab.


## Adding a final delivery stage to the pipeline
Add the final code snippet to your Jenkinsfile, and add a
`skipStagesAfterUnstable` option so your final Jenkinsfile looks like:
```
pipeline {
    agent none
    options {
        skipStagesAfterUnstable()
    }
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'python:2-alpine'
                }
            }
            steps {
                sh 'python -m py_compile sources/add2vals.py sources/calc.py'
                stash(name: 'compiled-results', includes: 'sources/*.py*')
            }
        }
        stage('Test') {
            agent {
                docker {
                    image 'qnib/pytest'
                }
            }
            steps {
                sh 'py.test --junit-xml test-reports/results.xml sources/test_calc.py'
            }
            post {
                always {
                    junit 'test-reports/results.xml'
                }
            }
        }
        stage('Deliver') {
            agent any
            environment {
                VOLUME = '$(pwd)/sources:/src'
                IMAGE = 'cdrx/pyinstaller-linux:python2'
            }
            steps {
                dir(path: env.BUILD_ID) {
                    unstash(name: 'compiled-results')
                    sh "docker run --rm -v ${VOLUME} ${IMAGE} 'pyinstaller -F add2vals.py'"
                }
            }
            post {
                success {
                    archiveArtifacts "${env.BUILD_ID}/sources/dist/add2vals"
                    sh "docker run --rm -v ${VOLUME} ${IMAGE} 'rm -rf build dist'"
                }
            }
        }
    }
}
```
The pipeline can be run from the Blue Ocean interface. The final artifacts can
be found under the `artifacts` tab.


