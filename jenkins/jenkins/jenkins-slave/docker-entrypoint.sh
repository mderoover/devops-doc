#!/bin/sh
# Based on https://github.com/danielguerra69/alpine-sshd/blob/master/docker-entrypoint.sh

if [ ! -f "/etc/ssh/ssh_host_rsa_key" ]; then
    # generate fresh rsa key
    ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
fi
if [ ! -f "/etc/ssh/ssh_host_dsa_key" ]; then
    # generate fresh dsa key
    ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
fi
if [ ! -f "/home/jenkins/.ssh/id_ed25519" ]; then
    # generate fresh ed25519 key
    ssh-keygen -f /home/jenkins/.ssh/id_ed25519 -N '' -t ed25519
fi
if [ ! -f "/home/jenkins/.ssh/authorized_keys" ]; then
    # generate authorized keys based on ed25519 key
    cp '/home/jenkins/.ssh/id_ed25519.pub' '/home/jenkins/.ssh/authorized_keys'
fi

# Prepare run dir
if [ ! -d "/var/run/sshd" ]; then
  mkdir -p /var/run/sshd
fi

# Give jenkins user permission to use docker in docker
addgroup docker
groupmod -og ${DOCKER_HOST_GROUP_ID:-666} docker
addgroup jenkins docker

exec "$@"
