# What is it
> Kubernetes (K8s) is an open-source system for automatic deployment, scaling,
> and management of containerized applications.

Kubernetes runs on **clusters** which are connected to work as a single unit.
**Containerized applications** can be deployed to such a cluster without tying
them to an individual machine.

A cluster exists out of a single **master** and multiple **nodes**. The master
is responsible for managing the cluster (scheduling, maintaining, scaling,
rolling out updates). The nodes serve as worker machines in the cluster. Each
node has a **Kubelet** which is responsible for communication with the
master. They also should have tools for handling containers such as docker. Each
production cluster should have 3 nodes as the bare minimum.

**Minikube** can be used to get started with Kubernetes. It offers a lightweight
VM with a simple cluster that you can run locally. The Minikube cluster only has
a single node.

Kubernetes needs to be told how to create and update applications you run in
your cluster. This is done using **Deployments**. Once you have a valid
Deployment, Kubernetes uses it to schedule your application to be run by the
nodes of the cluster. It also continues to monitor your application instances
and as soon as one fails or gets shutdown, it starts up a new instance. This
provides a measure of self-healing to your application.

When we create a deployment Kubernetes creates pods which in turn create
containers. Kubernetes does not create containers directly. Pods are the atomic
unit of Kubernetes. A pod can be seen as a grouping of one or more containers
and shared resources. These resources can be volumes, networking resources, meta
data, etc. The containers in a pod share an IP address and port space, are
always co-created and co-scheduled, and run in a shared context on the same
node.

Pods should be interchangeable even though they could have different IP
addresses and be running on different nodes. They can be lost when a node
shuts down and recreated by the master when needed. This all needs to be
transparent to your application. For this reason, Kubernetes has **Services**.
A Service provides a gateway between the virtual private network that spans
your cluster and the outside. A Service makes your application accessible from
outside the cluster. Services allow your applications to receive traffic.
Load balancing for your application instances is embedded in the Service.

Kubernetes is made to be **scalable**. You can manually scale using the Deployment
but autoscaling is also supported.



# Why should we use it
With modern web services, users expect applications to be available 24/7 while
developers expect to deploy new versions of those applications multiple times a
day. Containerization enables these applications to be released and updated in
an easy and fast way without any downtime.

Kubernetes makes those containers a run when and where you want and helps them
find the resources they need. It has been created with scalability in mind and.
They claim that you can't outgrow Kubernetes.

Kubernetes main advantages are **scalability** and **rolling updates**. You can
scale up to world-scale, and update without any downtime.


# How to use it
We will explain how to get a simple cluster up and running on your local
machine. In reality you want to run a more complicated but powerful cluster
either in the cloud, or spread across multiple local/virtual machines.

Cloud providers like [Google](https://cloud.google.com/kubernetes-engine) and
[Amazon](https://aws.amazon.com/eks/) provide their own Kubernetes cluster
services which you should use in a realistic scenario.


## Minikube cluster
This section details how to get a simple explanatory cluster with a single node
up and running.

To check whether virtualisation is supported on your local machine, run:
```
grep -E --color 'vmx|svm' /proc/cpuinfo
```
If the output is non-empty, virtualisation is supported.

First install **Kubectl**. On a Debian based system the build in packaging
system can be used for this. If you have a different system please look
[here](https://kubernetes.io/docs/tasks/tools/install-kubectl).
```
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
```
Kubectl is also available on *Snap* and *Homebrew*.

Verify your Kubectl installation using:
```
kubectl cluster-info
```
If you see a URL response, Kubectl is correctly configured.

Install a Hyervisor on your machine. Either
[KVM](https://www.linux-kvm.org/page/Main_Page) or
[VirtualBox](https://www.virtualbox.org/wiki/Downloads) should be good.

Install Minikube:
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
    && sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

Verify your installation of Minikube:
Note that `kvm2` is a valid alternative to `virtualbox`.
```
minikube start --driver=virtualbox
minikube status
```

If this has the following output everything is working fine:
```
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

You can stop the cluster using the following command:
```
minikube stop
```

If `minikube start` returns `machine does not exist` you need to clean
minikube's local state using `minikube delete`.


## Fetching cluster information
To interact with the cluster we will use **kubectl**, the command line
interface. To check whether it is installed correctly you can run:
```
kubectl version
```
Minikube should normally have kubectl automatically configured to use your
minikube cluster.

The most common kubectl commands are:
```
kubectl get         # list resources
kubectl describe    # show detailed information about a resorce
kubectl logs        # print the logs from a container in a pod
kubectl exec        # execute a command on a container in a pod
```

To get information about your cluster you can use the following commands:
```
kubectl cluster-info
kubectl get nodes
```
Kubectl uses the Kubernetes API to interact with your cluster. You can also
directly use this API yourself.est api


## Deploying an application on the cluster
You need to instruct Kubernetes how to create and update instances of your
application. This is done using a **Deployment**. This Deployment needs to know
which container it needs to run for your application and how many of these
instances you want to have up and running. This information can be changed later
to scale and update your application.

To create a new deployment run:
```
kubectl create deployment firstdeployment --image=<docker-image:tag>
```
This made your cluster look for a node on which to run the application, schedule
the application to run, and configure the cluster to reschedule your application
when needed.

You can get an overview of your deployments using:
```
kubectl get deployments
```

Note: Deployments are used to run stateless applications in your cluster. If you
application is statefull you need to use a **StatefulSet** instead. More about
this in the [Kubernetes
documentation](https://kubernetes.io/docs/tasks/run-application/run-replicated-stateful-application/)


## Getting information from a pod
To see which **pods** are running in your cluster you can use:
```
kubectl get pods
```

To get detailed information about your pods, like which containers are running
in them and what recent events happened, IP-addresses and ports, you can run:
```
kubectl describe pods
```

Pods running in a Kubernetes cluster are running on a private virtual network.
They are visible from within the cluster but not from outside. With kubectl
however you can create a proxy into your network. (You probably want to do this
in a second terminal)
```
kubecl proxy
```

The API server will automatically create an endpoint for each pod based on the
name of the pod. These APIs are also accessible through the proxy.
You can get a pod name using:
```
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo Name of the Pod: $POD_NAME
```

To see the output of this API you can use `curl`:
```
curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/
```
Note: If you want the pod APIs to be available without the proxy, a Service is
required.

To get logs from your containers in a pod you can use:
```
kubectl logs $POD_NAME
```
If you have multiple containers in a pod you also need to specify the container
name.

It is also possible to directly execute command inside a container running in a
pod (again, when there are multiple containers you also need to specify the
container name):
```
kubectl exec -ti $POD_NAME bash     # start a bash session inside a container
```


## Exposing your application
**Services** are what enables your pods to receive traffic. The preferred way to
define a service is using *yaml* although *json* is also possible. Services
match a set of pods based on labels and selectors.

There are multiple types of Services with respect to how they are exposed:
1. ClusterIP (default): Expose on an internal IP in the cluster, only reachable
   inside the cluster.
2. NodePort: Expose the service on the same port of each selected node in the
   cluster using NAT. The Service is accessible from outside using
   *NodeIP* : *NodePort*.
3. LoadBalancer: Creates an external load balancer in the current cloud (if
   supported) and assigns a fixed external IP to the Service.
4. ExternalName: Exposes the Service using an arbitrary name by returning a
   CNAME record with the specified name. This type requires v1.7 or higher of
   `kube-dns`.

To list the running Services run:
```
kubectl list services
```

To create a new Service run:
```
kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
```

You can get information about a Service (like what external port is assigned to
it) using:
```
kubectl describe services/kubernetes-bootcamp
```

It is also possible to capture the external IP in a variable:
```
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT
```

The specified Service is now accessible from outside the cluster using `curl`:
```
curl $(minikube ip):$NODE_PORT
```

To delete a service you use the following syntax:
```
kubectl delete service -l run=kubernetes-bootcamp
```
Note that the application is still running after this command. It merely removes
is entry to the outside. To remove the application you will have to delete the
Deployment linked with it.


## Labels and selectors
**Labels** are key values pairs you can use to describe your Kubernetes objects.
It is good practice to use labels as much as possible since they describe meta
data about your objects.  When you create a Deployment, Kubernetes automatically
labels the created pod for you. You can see this label using:
```
kubectl describe deployment
```

You can use labels during the various commands and actions of Kubernetes. For
example to only get pods that have a certain label and list existing services:
```
kubectl get pods -l run=kubernetes-bootcamp
kubectl get services -l run=kubernetes-bootcamp
```

The following syntax is used to add a new label to a pod:
```
kubectl label pod $POD_NAME app=v1
```

With **selectors** it becomes possible to assign pods to a node with a specified
label (Note that Affinity and anti-Affinity can also be used for this). For more
information, check the documentation of Kubernetes.


## Scaling your application
**Scaling** is accomplished by increasing the number of replicas in a
Deployment. Autoscaling is also a possibility but for this you need to delve
into the Kubernetes documentation. Scaling to zero is equivalent to disabling
all nodes.

Services have an integrated **load-balancer** which will distribute the traffic
across the various instances of an application.

To scale your deployment use the following syntax:
```
kubectl scale deployments/kubernetes-bootcamp --replicas=4
```


## Updating your application
To have **rolling updates** without downtime, you need to have multiple
instances of the application running.

To update the image used by an application you use the following syntax:
```
kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=jocatalin/kubernetes-bootcamp:v2
```
Kubernetes automatically starts new pods with the new version while terminating
the old pods. Load balancing ensures the update can happen without any downtime.

To get confirmation about an update you can use:
```
kubectl rollout status deployments/kubernetes-bootcamp
```

To rollback to the previous version:
```
kubectl rollout undo deployments/kubernetes-bootcamp
```


## Using Kubernetes with yaml
Yaml can be used to describe Kubernetes objects.

A Deployment object for example can be described using yaml:
``` yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webapp1
  template:
    metadata:
      labels:
        app: webapp1
    spec:
      containers:
      - name: webapp1
        image: katacoda/docker-http-server:latest
        ports:
        - containerPort: 80
```

A Service object can be described using yaml:
``` yaml
apiVersion: v1
kind: Service
metadata:
  name: webapp1-svc
  labels:
    app: webapp1
spec:
  type: NodePort
  ports:
  - port: 80
    nodePort: 30080
  selector:
    app: webapp1
```

Such yaml files can be deployed on the cluster using:
```
kubectl create -f deployment.yaml
kubectl create -f service.yaml
```

To edit an exisitng Kubernetes object using yaml you can use:
```
kubectl edit <object>
```


# References
* [Getting Started](https://kubernetes.io/docs/tutorials/kubernetes-basics/)
* [Try out Kubernetes](https://www.katacoda.com/courses/kubernetes)
* [Alt Kataconda tutorials](https://www.katacoda.com/contino/courses/kubernetes)
* [Configuration best
    practices](https://kubernetes.io/docs/concepts/configuration/overview/)
* [Reference](https://kubernetes.io/docs/reference/)

## Extra third-party tools
* [Forge](https://forge.sh/) - Define and deploy multi-container apps on
  Kubernetes from source
* [Helm](https://helm.sh/) - Helm is the best way to find, share, and use
  software build for Kubernetes
