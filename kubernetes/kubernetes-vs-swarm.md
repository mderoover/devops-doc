# Kubernetes VS Swarm

## Intro

Both kubernetes and docker swarm are container orchestration tools which manages containers that need to be deployed.
This means that they will handle the setup of containers, try to manage failures and so on.

Docker Swarm is a tool that is created by Docker itself and is loosly based on the docker compose concepts.
It uses the same file type (Yaml) to define the layout of the deployment that needs to be made.
Since this technology is only used for docker containers, it is fairly biased on how it should be used and deployed.
This also means that you can do less with it, but also do less wrong.

Kubernetes on the ohter hand can be used for other containerisation tools than docker.
Therefore, it is less opiniated on how it should be used an deployed to a cluster.
This also means that it is a very difficult tool to learn since it is not always clear what is the best method for achieving the goal

## Comparison Table

In this section a comparison will be given with the help of a table to differentiate the most important features of both technologies.

| Feature                   | Kubernetes                                           | Docker Swarm                                        |
|---------------------------|------------------------------------------------------|-----------------------------------------------------|
| main container technology | unopiniated                                          | Docker                                              |
| ease of use               | hard                                                 | easy                                                |
| ease of installation      | difficult, a lot of configuration (if done manually) | easy                                                |
| vendor backing            | yes                                                  | yes                                                 |
| platform compatibility    | linux                                                | a lot                                               |
| scalability               | both horizontal and vertical                         | both horizontal and vertical                        |
| failover                  | multi master setup that can co-operate with failure  | multi master setup that can co-operate with failure |

## Pros & Cons

### Docker Swarm

* Pro:
  + build-in in Docker
  + uses little more resources than docker itself
  + is fairly simple, perfect tool to learn orchestration.
  + has implemented 20% of the features to cover 80% of the use cases
  + is build by developpers for developpers.
  + it run on a lot of platforms
  + easier to troubleshoot
* Cons:
  + has implemented 20% of the features to cover 80% of the use cases
  + can only orchestrate docker container (seems logic)
  + is less popular than kubernetes

### Kubernetes

* Pro:
  + Can use multiple containerisation tools (Not at the same time)
  + is unopiniated, can do a lot and does not take sides of how something should be done
  + has a lot of commumnity support, is a well known technology
  + cloud vendor support is huge
* Cons:
  + is unopiniated, can do a lot and does not take sides of how something should be done
  + is often used because of the popularity, even when not required
  + it was orriginally ment to use as a in between solution for other tools to work with, but is most of the time used as a standalone tool.
  + depends on a lot of external tools.

## Conclusion

Choosing for the one or the other is not as simple as an oneliner.
Kubernetes is a highly popular product that is well known in the industry right now.
However, it is often used in situation that do not need the complexity of kubernetes.
Docker on the other hand is a build in feature of the docker ecosystem and can handle a lot of the situations of what Kubernetes is used for.
Due to being less popular, it often is overlooked when it could be usefull.

Kubernetes is not a bad choice however. It has a lot more flexibility when setting up complex systems.
Therefore, Kubernetes is a valid choice when working on more complex systems that are handled by bigger teams.

## References

* [Docker Swarm Feature Highlights](https://docs.docker.com/engine/swarm/)
* [Kubernetes - Getting Started](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)
* [Udemy - Docker Mastery Course](https://www.udemy.com/course/docker-mastery/) - Especially the docker swarm vs kubernetes lecture
