# Summary of tools that can be used in a CI/CD world

## Continious integration automation tools

These tools are able to setup a pipeline for autonomous software building and
testing. Such a pipeline is a series of actions that can be executed in series
to automatically build, test and deploy the written software. The main advantage
is that all these repetitive actions can be programmed to be done by a computer
and let the programmers continue improving the software.

### Jenkins

* Basic Info
  + Very mature CI tool that let you automate the process of building, testing
    and more on a git repo or other tool
  + Is a bit older, but has a lot of features (plugins)
  + Free (open source)


### Gitlab CI

* Basic Info
  + All in one tool for software development. It contains the basics as a git
    frontend with merge requests, but has also build in CI capabilities.
  + Fairly common tool to use nowadays.
  + Free (open source - community edition)


### TeamCity

* Basic Info
  + CI tool from JetBrains
  + Written in and for java
    (can build for other technologies too, but is especially focussed on java)
  + Has a lot of plugins.
  + Free (proprietary)

### Drone CI

* Basic Info
  + Container native tool written in Go.
  + Supports ARM
  + One of the most recent tools out there, but not immature
  + Free (open source - community edition)

### Travis CI

* Basic Info
  + Defacto standard with GitHub.
  + Allows multi-platform testing at the same time. Something most other tools cannot do.
  + Free for open source projects on github.

### Circle CI

* Basic Info
  + Claims to be the fasted tool.
  + Works on UNIX OSes and windows
  + Lot of features
  + Very basic free version (not self hosted) (self hosted version is paid)


### References
 * [Best CI tools - cuelogic](https://www.cuelogic.com/blog/best-continuous-integration-ci-tools)
 * [Best CI tools comparison - browserstack](https://www.browserstack.com/blog/best-ci-cd-tools-comparison/)

## Cloud Infrastructure as a Service

Cloud IaaS companies build physical server clusters to rent them out to other
users. This gives the advantage that you do not need to physically maintain a
cluster to deploy the created application onto. Also, it is much easier to
quickly upscale when there is an increase in demand for resources.

### AWS

* Basic Info
  + Launched in 2006
  + Hosting worldwide: 70 + 16 AZ
  + Market share: 40%
  + Services: 200+
  + Max downtime ever recorded: 3h09 in 2014

* Takeaway Messages:
  + Pro's:
    - Dominates cloud domain with features such as configuration, monitoriing, security, auto-scaling, etc.
    - Better offerings
    - More experience, enterprise friendly services
    - More opensource tools integration
    - Global reach
  + Con's:
    - Difficult to choose, a lot of services and can be confusing
    - Overwhelming options of services
    - Cost management is not that good

* Intro
  + Amazon web services
  + Global cloud platform 
  + Infrastructure as a service 
  + Platform as a service
  + Software as a service 
  + Clould storage platform

* Why Popular:
  + Per hour billing
  + Easy sign up service
  + Simple billing
  + Stability
  + Trusted vendor

* Popular Services:
  + EC2 (custom server)
  + VPC: virtrual private cloud
  + S3: simple storage service
  + RDS: relational database
  + route 53: DNS service
  + ELB: Elastic Load Balancer
  + Autoscaling


* Interesting Facts:
  + [AWS makes their own routes for the servers](https://www.geekwire.com/2017/amazon-web-services-secret-weapon-custom-made-hardware-network/)

### GCP

* Basic Info:
  + Launched in 2011
  + Hosting worldwide: 67+ AZ
  + Market share: 10%
  + Services: 60+
  + Max downtime ever recorded: 14 minutes in 2014

* Takeaway Messages:
  + Pro's
    - Expertise in devops
    - Flexible discounts & contracts
    - Specifically designed for cloud based businesses
  + Con's:
    - Less efficient management tooling
    - Less enterprice ready

* Why Popular:
  + Cost effective
  + Highly scalable
  + Custom machine types
  + Internet of things
  + API platform and ecosystem
  + Big data analytics
  + Cloud ai
  + Serverless

* Popular Services:
  + Computing and hosting
    - Serverless computing
    - Application platform
    - Containers
    - Virtual machines
    - Combining computing and hosting options
  + Storage
  + Databases
  + Networking
    - Networks, firewalls and routes
    - Load balancing
    - Cloud DNS
    - Advanced connectivity
  + Big data
    - Data analysis
    - Batch and streaming data processing
    - Asynchronous messaging
  + Machine learning
    - Machine learning APIs
    - AI platform


### Microsoft Azure

* Basic Info:
  + Launched in 2010
  + Hosting worldwide: 54 AZ
  + Market share: 30%
  + Services: 100+
  + Max downtime ever recorded: 39h77 in 2014

* Takeaway Messages:
  + Pro's:
    - More reliable when it comes to integrating with microsoft products
    - Ranks first in the development & testing tools
    - Also provides hybrid clould
  + Con's:
    - Entered late in IAAS market
    - Less data centres
    - Fewer services & features

* Why Popular:
  + Microsoft oriented


### Digital Ocean

* Basic Info:
  + Started in 2011
  + Hosting worldwide
  + Fast growth in 2013, becoming one of the big players.
  + Limited set of services, but all the basics are available.
  + Became popular with the ssd storage solution.

* Takeaway Messages:
  + Pro's:
    - Kubernetes certified
    - Fast ssd storage
  + Con's:
    - Less popular 
    - Less features than other cloud providers


### IBM Cloud

* Basic Info:
  + Started in 2007 in collaboration with google to provide cloud instances for
    universities.
  + Not  that well known.

### Oracle Cloud

* Basic Info:
  + Started in 2011??
  + Not a lot of info to find


## Custom cloud software

This kind of software is software dedicated to deploy on bare-metal machines that are meant to run in a cluster. These tools will manage the resources in the cluster like compute, storage and networking.

### OpenStack

* What is OpenStack:
  + OpenStack is a set of software tools for building and managing cloud
    computing platforms for public and private cloulds
  + OpenStack is managed by the OpenStack foundation, a non-profit that oversees
    both development and  community-building around the project
  + OpenStack is the future of cloud computing backed by some of the biggest companies.
  + Created in 2010 by rackspace (hosting company)
  + Software to create your own infrastructure to host your applications


## Logging and Visualization tools

These tools collect data (for example logs) and save them in a centralized manner. Furthermore, they are capable of nicely visualizing the gathered data.

### ELK stack

* Basic Info
  + Combination of multiple individual tools:
    - Logstash: Log interpreter tool
    - Elasticsearch: Database that stores all the interpreted data
    - Kibana: Frontend visualization tool that shows the received data

## Configuration Managment

Configuration management tools are tools that automate certain tasks on multiple nodes inside a cluster. These kind of frameworks aim to reduce the time that is spend on configuring multiple nodes on the network.

### Ansible

* Basic Info
  + Python written configuration management tool for organizing a cluster setup.
  + Uses playbooks for configuring servers.
    - Playbook consist of set of configuration settings that are needed on each node
      of a cluster.
    - Playbook commands are pushed to all the servers to configure them
    - response is returned to the runner of the playbook.
      (Sender does not need to be a master of the cluster, can be a remote
       workstation that pushes the commands)
    - Keeps track of changes, if something is already installed, it will not try to
      reinstall for example.
    - Example:
    ``` yaml
    ---
    - hosts: webservers
        vars:
            http_port: 80
            max_clients: 200
        remote_user: root
        tasks:
        - name: ensure apache is at the latest version
            yum:
            name: httpd
            state: latest
        - name: write the apache config file
            template:
            src: /srv/httpd.j2
            dest: /etc/httpd.conf
            notify:
            - restart apache
        - name: ensure apache is running
            service:
            name: httpd
            state: started
        handlers:
            - name: restart apache
            service:
                name: httpd
                state: restarted
    ```
  + Communicates over ssh to other servers to fulfill requrements.
  + No need for client software on each node of the network.
    - SSH and python are required however...
  + Communication is save since it uses SSH
  + Free (open source)


### puppet

* Basic Info
  + Master node that contains the main configuration and pushes the config to other nodes.
  + Needs client software on all the nodes.
  + Easier to program, but limited features.
  + Secure communication that uses self signed certificates by the puppet server
  + Uses DSL for configuring.
    - Example:
    ``` puppet
    case $operatingsystem {
        centos, redhat: { $service_name = 'ntpd'}
        debian, ubuntu: { $service_name = 'ntp'}
    }
    
    package { 'ntp':
        ensure => installed,
    }
    
    service { 'ntp':
        name => $service_name,
        ensure => running,
        enable => true,
        subscribe => File['ntp.conf'],
    }
    ```
  + Free (open source)


### chef

* Basic Info
  + Master node that contains the main configuration and pushes the config to other nodes.
  + Needs client software on each node
  + Writting in Ruby
  + Somewhat harder to configure, but a lot of features.
    - Uses plain ruby to configure the systems.
  + Free (open source)